from django.shortcuts import render

# Create your views here.

def webpage1(request):
    return render(request, 'home/welcomepage.html')

def webpage2(request):
    return render(request, 'home/jquery.html')